#http://archive.ics.uci.edu/ml/datasets/heart+Disease


rm(list = ls())                                       #rimuovi dati salvati in memoria         
  
setwd("/home/david/Scrivania/heart")                 #imposta la directory

library("e1071")                                      #libreria per il modello Naive Bayes
library(rpart)                                        #libreria per decision tree
library(rpart.plot)                                   #libreria per rappresentare l'albero
library(caret)                                        #per matrice di confusione e altro
library(randomForest)                                 #libreria per randomforest
library(pROC)                                         #libreria per la cruva ROC e AUC

# 1) Lettura del dataset online. Viene creato come dataframe

life_heart = read.csv(file="heart.csv", header=FALSE, sep=",")

# 2) Vediamo il dataframe

life_heart

#age 

#sex 

#cp: chest pain type 
# -- Value 1: typical angina 
# -- Value 2: atypical angina 
# -- Value 3: non-anginal pain 
# -- Value 4: asymptomatic 

#trestbps: resting blood pressure (in mm Hg on admission to the hospital) 

#chol: serum cholestoral in mg/dl 

#fbs: (fasting blood sugar > 120 mg/dl) 
# -- Value 0: false
# -- Value 1: true

#restecg: resting electrocardiographic results 
# -- Value 0: normal 
# -- Value 1: having ST-T wave abnormality (T wave inversions and/or ST elevation or depression of > 0.05 mV) 
# -- Value 2: showing probable or definite left ventricular hypertrophy by Estes' criteria 

#thalach: maximum heart rate achieved 

#exang: exercise induced angina
# -- Value 0: no
# -- Value 1: yes

#oldpeak: ST depression induced by exercise relative to rest 

#slope: the slope of the peak exercise ST segment 
# -- Value 1: upsloping 
# -- Value 2: flat 
# -- Value 3: downsloping 

#ca: number of major vessels (0-3) colored by flourosopy 

#thal: 3 = normal; 6 = fixed defect; 7 = reversable defect 






# 3) Il dataframe non ha il nome delle colonne, che aggiungiamo

names(life_heart) <- c( "age", "sex", "cp", "trestbps", "chol","fbs", "restecg",
                        "thalach","exang", "oldpeak","slope", "ca", "thal", "num")

#    Sono indicate tutte variabili relative allo stato di salute di alcuni pazienti. La 
#    variabile d'interesse, ossia il nostro TARGET sar? "num" che indicher? il numero
#    d'infarti per ogni paziente.




# 4) Utilizziamo questo comando per vedere una panoramica del dataframe, in questo caso solo
#    i primi tre records. Utile soprattutto per i grossi dataset come nei Big Data.

head(life_heart,3)    




# 5) Per ottenere informazioni sulle dimensioni del dataframe

dim(life_heart)       

#fornisce numero righe e numero colonne. Non ? grande, ma potr? essere utile per comprendere
#come funziona un modello





# 6) Vogliamo studiare quali sono le caratteristiche che comportano per un paziente di subire
#    un infarto. Quindi, dove il numero dei infarti ? maggiore di 0 si inserir? "1" altrimenti
#    0

life_heart$num[life_heart$num > 0] <- 1




# 7) Vediamo i NA. 

is.na(life_heart)       #non ci permette di capire bene se ci sono o meno NA

which(is.na(life_heart[13]))  #con il comando which possiamo vedere in quale record sono presenti
                              #indicandone il numero della variabile. In questo caso possiamo 
                              #vedere che nella variabile 13, non ci sono NA.



sum(is.na(life_heart))      #Vediamo in tutto il dataset quanti c e ne sono
                            #non ci sono NA
                                 

#Il dataset ? abbastanza pulito, quindi finiscono qua le operazioni di pulizia del dataset.



# 8) Formiamo il training set, che servir? per formare il modello, e il testing set che
#    sar? utilizzato per testare il modello. Il training sar? formato dai 2/3 dei records
#    che compongono il dataset, scelti casualmente, invece il restante 1/3 costituir? il 
#    testing set.


set.seed(10)
r_casuali <- createDataPartition(life_heart$num,p=2/3,list=FALSE)
                        #scelta casuale della posizione dei records
r_casuali
training <- life_heart[r_casuali,]
testing <-  life_heart[-r_casuali,]




#  9) Proviamo qualche modello

AUC = list()
ACC = list() 
                    #creiamo due liste dove insereremo per ogni modello AUC e l'accuracy,
                    #per poter valutareil migliore.

#---------------DECISION TREE---------------------------

f="as.factor(training$num) ~ ."            #formula, come output vogliamo il numero infarti e come
                                #input prendiamo tutte le altre variabili

tree = rpart(formula=f, data = training, method ='class',parms = list(split = "gini"))
                                #indichiamo la formula, il dataset training dove sar?
                                #allenato il modello, il metodo class che indica che
                                #siamo facendo una classificazione

summary(tree)                   #vediamo alcune informazioni e la rappresentazione grafica
rpart.plot(tree, type=3, extra=100, shadow.col="gray",fallen.leaves=FALSE)  


pred.tree = predict( tree, testing[,-ncol(testing)], type="class" ) 
                                #effettuo la previsione sul dataset testing togliendogli
                                #l'ultima colonna che ? la variabile num

ConfMat_tree <- confusionMatrix(pred.tree, testing[,"num"])
                                #ne determino la matrice di confusione, mettendo a confronto
                                #i risultati previsti con quelli reali. 
ConfMat_tree

ConfMat_tree$table               #La matrice di confusione

ACC$tree = ConfMat_tree$overall[1]          #vediamo l'accuracy


                                #l'accuracy ? un p? maggior del 73%,
                                #dato dal rapporto dei record classificati bene sul
                                #totale dei records.
 
AUC$tree <- roc(as.numeric(testing$num),as.numeric(as.matrix((pred.tree))))$auc
                                #area sotto la curva ROC

#---------------MODELLO LOGISTICO---------------------------

LG_full = glm(formula=f,  family=binomial(link="logit"), data= training);
                              #diamo la formula f anche in questo codice


summary(LG_full);             #vediamo informazioni sul modello
anova(LG_full, test="Chisq")




                               #facciamo la predizione sul testing set 
pred.glm_0=predict(LG_full,newdata=testing,type='response')
                                # probabilit? di output nella forma di P(y=1|X).

pred.glm <- ifelse(pred.glm_0 > 0.5,1,0)
                               #Arrotonda i valori maggiori di 0.5 in 1 altrimente
                               #mette 0


                               #matrice di confusione
ConfMat_log <- confusionMatrix(pred.glm, testing[,"num"])
                               #come si pu? notare il modello ha una migliore accuracy
ACC$log = ConfMat_log$overall[1]

AUC$log <- roc(as.numeric(testing$num),as.numeric(as.matrix((pred.glm))))$auc
#area sotto la curva ROC


#-------------RANDOM FOREST--------------

rf = randomForest(as.factor(training$num) ~., data=training,
                  ntree=500,
                  mtry = 3, # number of variables sampled 
                  importance=TRUE,na.action=na.roughfix)


#Info sulla random forest
print(rf)

#predizione
pred.rf = predict( rf, newdata=testing[,-ncol(testing)], type='prob')[,2]
pred.rf = as.numeric(pred.rf > .5 ) # classifier

#matrice di confusione
ConfMat_rf <- confusionMatrix(pred.rf, testing[,"num"])
#come si pu? notare il modello ha una migliore accuracy
ACC$rf = ConfMat_rf$overall[1]

AUC$rf <- roc(as.numeric(testing$num),as.numeric(as.matrix((pred.rf))))$auc
#area sotto la curva ROC




#fornisce quali variabili sono piu importanti, in base a come decresce l'accuracy
#mediamente nel caso togliessimo la variabile per la quale stiamo valutando l'importanza.

importance(rf,type=NULL, class=NULL, scale=TRUE) 
varImpPlot(rf,type=1, class=NULL, scale=TRUE, top=10,sort = TRUE)




#---------------NAIVE BAYES---------------------------

model <- naiveBayes(as.factor(training$num) ~., data = training)
                               #mettiamo la formula convertendo la variabile target
                               #in variabile factor, dato che dovr? assumere solo valore
                               # 1 o 0

print(model)                   #vediamo le probabilit? condizionate e a priori calcolate.
                               #per le variabili quantitative si ipotizza che si distribuiscono
                               #come una normale, e le probabilit? saranno prese da questa
                               #distribuzione

pred.nb <- predict(model, testing[,-ncol(testing)],type="class")
pred.nb                        #effettuiamo la previsione



                               #matrice di confusione
ConfMat_nb <- confusionMatrix(pred.nb, testing[,"num"])

ACC$nb = ConfMat_nb$overall[1]
                              #il modello presenta un buon livello d'accuracy

AUC$nb = roc(as.numeric(testing$num),as.numeric(as.matrix((pred.nb))))$auc
#area sotto la curva ROC





# 10) Valutazione modello

ACC
AUC
       #Vediamo che per entrambi i due parametri, il Naive Bayes risulta essere migliore





# 11)
#----------------CONTRATTO DREAD DISEASE---------------------


#calcolo probabilit? dread disease p^{dd}

model_prob <- naiveBayes(as.factor(life_heart$num) ~., data = life_heart)

#per il calcolo delle probabilit?, dato che non stiamo piu effettuando della classificazione
#e non stiamo risolvendo un problema di machine learning, possiamo utilizzare tutto il dataset
#e non il training, in modo d'avere.

#Calcoliamo il premio unico di un contratto dread disease che copre solo il rischio d'infarto
#del miocardio. Si ipotizza che il contratto copra solo un periodo limitato, ossia di 20 anni.
#L'assicurato di riferimento ha 50 anni e registra alcuni valori che abbiamo visto nel
#dataset life_heart. Questi valori saranno utilizzati dalla compagnia per determinare
#le probabilit? di passare dallo stato di attivo a quello di invalido per infarto.
#Dato che la compagnia assicurativa ha bisogno anche dei suoi valori
#futuri che tuttavia non possiede, li dovr? determinare con modelli estrapolativi. Per 
#semplicit?, in questo lavoro si ? realizzato un dataset, chiamato Rossi, dove sono stati
#inseriti questi valori ipotetici.
#Ipotizziamo un tasso di sconto del 5%.

Rossi = read.csv(file="Rossi.csv", header=TRUE, sep=",")
names(Rossi) <- c( "age", "sex", "cp", "trestbps", "chol","fbs", "restecg",
                        "thalach","exang", "oldpeak","slope", "ca", "thal","hpx")
Rossi

# Il premio quindi ? dato dalla somma da 0 a 20 anni del prodotto della probabilit? 
#che d'attivo resta attivo con la probabilit? che transita allo stato d'invalido e per
#finire con il fattore di sconto.
#
#sum_{h=0}^{20} hpx(aa) * wx+h * (1+i)^{-(h+0.5)}
#
#Nel dataset sono state calcolate le probabilit? hpx(aa) come le probabilit? di sopravvivere
#h anni di una testa assicurativa di x anni.
#
#Quindi abbiamo hpx(aa). Il fattore di sconto sar?

h=c(1:20)             #periodo da 1 a 20

vh=(1+0.05)^(-(h+0.5)) #fattore di sconto

vh

#Ora dobbiamo calcolare le wx+h. In questo caso, utilizzeremo le probabilit? generate
#dal modello Naive Bayes che abbiamo addestrato prima


                              #prendiamo solo le prime 13 variabili, che servono per il modello
                              #e invece di fare type="class", mettiamo "raw", che fornir? le
                              #probabilit?.

prob.nb <- predict(model_prob, Rossi[1:13],type="raw")
prob.nb

                         #noi siamo interessati solo alla probabilit? che un individuo
                         #passi dallo stato attivo allo stato di colpito da infarto, quindi
                         #solo la seconda colonna
wxh=prob.nb[,2]

#Il tasso di premio unico quindi ? dato dalla somma dei tre prodotti

P=sum(Rossi[,14]*vh*wxh)

P







